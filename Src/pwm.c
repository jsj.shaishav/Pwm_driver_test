/*
 * pwm.c
 *
 *  Created on: 01-Jun-2018
 *      Author: shais
 */

#include "pwm.h"

TIM_HandleTypeDef htim3;



void MXX_TIM3_Init(int frequency,int ch1,int ch2,int ch3,int ch4)
{
  uint32_t period = (uint32_t)((24000000/frequency)-1);
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = (uint32_t)((SystemCoreClock/24000000) - 1);
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = period;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  HAL_TIM_PWM_Init(&htim3);
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = period/2;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1);
  sConfigOC.Pulse = period/2;
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);
  sConfigOC.Pulse = period*((uint32_t)(ch3/100));
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3);
  sConfigOC.Pulse = period*((uint32_t)(ch4/100));
  HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4);
  HAL_TIM_MspPostInit(&htim3);
}
void pwmStart(int Channel)
{		switch (Channel)
	   {
	       case 1: HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	               break;
	       case 2: HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	                break;
	       case 3: HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
	               break;
	       case 4: HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);
	                break;
	   }

}

