/*
 * pwm.h
 *
 *  Created on: 01-Jun-2018
 *      Author: shais
 */

#ifndef PWM_H_
#define PWM_H_
#include "main.h"
#include "stm32f3xx_hal.h"
void MXX_TIM3_Init(int frequency,int ch1,int ch2,int ch3,int ch4);
void pwmStart(int Channel);


#endif /* PWM_H_ */
